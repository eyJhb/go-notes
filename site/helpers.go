package site

import (
	"bytes"
	"crypto/sha1"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
)

func makePlantuml(data []byte) ([]byte, error) {
	cmd := exec.Command("plantuml", "-tsvg", "-pipe")
	cmd.Stdin = bytes.NewReader(data)

	var out bytes.Buffer
	cmd.Stdout = &out
	var outErr bytes.Buffer
	cmd.Stderr = &outErr

	err := cmd.Run()
	if err != nil {
		log.Printf("Failed to make diagram, with error %s: ", outErr.String())
		return nil, err
	}

	return out.Bytes(), nil
}

func makeAsy(data []byte) ([]byte, error) {
	// create tmp file
	f, err := ioutil.TempFile("", "asy*.svg")
	if err != nil {
		log.Fatal(err)
	}
	fileName := f.Name()
	defer os.Remove(fileName)
	f.Close()

	// exec command
	cmd := exec.Command("asy", "-outformat", "svg", "-o", fileName, "-")
	cmd.Stdin = bytes.NewReader(data)
	var outErr bytes.Buffer
	cmd.Stderr = &outErr

	err = cmd.Run()
	if err != nil {
		log.Printf("Failed to make diagram, with error %s: ", outErr.String())
		return nil, err
	}

	// read file
	return ioutil.ReadFile(fileName)
}

func generateSha1(b []byte) string {
	h := sha1.New()
	h.Write(b)
	bs := h.Sum(nil)

	return fmt.Sprintf("%x", bs)
}
