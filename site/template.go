package site

import (
	"html/template"
	"strings"
)

const (
	baseTemplate = `{{define "base"}}
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>{{.Title}}</title>

    <!-- <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script> -->
    <script id="MathJax-script" async
        src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
    </script>

    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit-icons.min.js"></script>
</head>
<body>
    <nav class="uk-navbar-container" uk-navbar>
        <div class="uk-navbar-left">
            <a href="/" class="uk-navbar-item uk-logo">{{.Title}}</a>

            <ul class="uk-navbar-nav">
                {{range $page := .RootPages}}
                    <li>
                        <a href="/{{replaceMd $page.Path}}">{{$page.Title}}</a>
                    {{if $page.Pages}}
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                            {{range $subpage:= $page.Pages}}
                                <li><a href="/{{replaceMd $subpage.Path}}">{{$subpage.Title}}</a></li>
                            {{end}}
                            </ul>
                        </div>
                    {{end}}
                    </li>
                {{end}}
            </ul>
        </div>
    </nav>
    <div uk-grid>
        <div class="uk-width-1-6">
             <ul class="uk-nav uk-nav-default">
                {{template "docheadings" .Headings }}
            </ul>
        </div>

        <div class="uk-width-5-6">
            {{ .Content }}
        </div>
</body>
</html>
{{end}}
{{define "docheadings"}}
    {{if .}}
        <ul class="uk-nav-sub">
        {{range .}}
            <li>
                <a href="#{{.ID}}">{{.Title}}</a>
                {{template "docheadings" .Headings }}
            </li>
        {{end}}
        </ul>
    {{end}}
{{end}}`
)

func replaceMd(input string) string {
	return strings.Replace(input, ".md", ".html", -1)
}

func readyTemplate(tmpl string) map[string]*template.Template {
	// functions map for tmpl
	funcMap := template.FuncMap{
		"replaceMd": replaceMd,
	}

	// map template
	mapTmpls := make(map[string]*template.Template)

	// ready template
	if tmpl == "" {
		mapTmpls["base"] = template.Must(template.New("").Funcs(funcMap).Parse(baseTemplate))
	} else {
		mapTmpls["base"] = template.Must(template.New("").Funcs(funcMap).ParseFiles(tmpl))
	}

	return mapTmpls
}
