package site

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"strings"

	blackfriday "gopkg.in/russross/blackfriday.v2"
)

var (
	mdExtensions = blackfriday.CommonExtensions | blackfriday.Footnotes | blackfriday.AutoHeadingIDs
)

type Heading struct {
	ID       string
	Title    string
	Headings []Heading
}

type Page struct {
	Title      string
	Path       string
	Assets     []string
	ByteAssets map[string][]byte
	Pages      []Page

	s *Site

	haveReadFile bool
	fileBytes    []byte
}

func (p *Page) readFile() ([]byte, error) {
	if p.haveReadFile {
		return p.fileBytes, nil
	}

	if p.Path == "" {
		p.haveReadFile = true
		return []byte{}, nil
	}

	b, err := ioutil.ReadFile(p.s.Input + p.Path)
	if err != nil {
		return []byte{}, err
	}

	p.haveReadFile = true
	p.fileBytes = b
	return b, nil
}

func (p *Page) Markdown() ([]byte, error) {
	return p.readFile()
}

func (p *Page) HTML(prefix string) ([]byte, error) {
	// init byteAssets
	p.ByteAssets = make(map[string][]byte)

	// read file
	b, err := p.readFile()
	if err != nil {
		return []byte{}, err
	}

	// there is nothing is this file
	// so lets just generate a index
	if len(b) == 0 {
		b = p.generateTOC(prefix, 0)
	} else if bytes.Contains(b, []byte(`<!-- generate-tec -->`)) {
		b = append(b, p.generateTOC(prefix, 0)...)
	}

	return blackfriday.Run(b,
		blackfriday.WithExtensions(mdExtensions),
		blackfriday.WithRenderer(newCustomRenderer(p)),
	), nil
}

func (p *Page) generateTOC(prefix string, level int) []byte {
	if len(prefix) > 0 && prefix[0] == '/' {
		prefix = prefix[1:] + `/`
	}

	rStr := fmt.Sprintf("%s - [%s](/%s)\n", strings.Repeat("\t", level), p.Title, strings.Replace(prefix+p.Path, ".md", ".html", 1))

	for _, page := range p.Pages {
		rStr += string(page.generateTOC(prefix, level+1))
	}

	return []byte(rStr)
}

func (p *Page) Headings() ([]Heading, error) {
	md := blackfriday.New(blackfriday.WithExtensions(mdExtensions))

	// read file
	b, err := p.readFile()
	if err != nil {
		return []Heading{}, err
	}

	node := md.Parse(b)
	if node == nil {
		return []Heading{}, errors.New("Could not parse node, at section: " + p.Path)
	}

	var h Heading
	var levels []int
	node.Walk(func(n *blackfriday.Node, entering bool) blackfriday.WalkStatus {
		// if not heading or if we are not entering, then GoToNext
		if n.Type != blackfriday.Heading || entering {
			return blackfriday.GoToNext
		}

		// get base information
		title := string(n.FirstChild.Literal)
		ID := n.HeadingData.HeadingID
		level := n.HeadingData.Level - 1 // level start at 1, slices at 0

		// ensure we have enough slots in our levels slice
		// initial value is -1, so that our increment increases
		// it to 0
		for i := len(levels); i <= level; i++ {
			levels = append(levels, -1)
		}

		// erase any levels lower then our current level
		for i := level + 1; i < len(levels); i++ {
			levels[i] = -1
		}

		// always += 1 to our current level, which will increment
		// the level we are on, so e.g.
		// # 1. First Header [0]
		// ## 1.1. Second Header [0,0,0]
		// ### 1.1.1. Thrid Header [0,0,0]
		// # 2. Secound first header [1,0,0]
		levels[level] += 1

		// loop backwards to ensure that if
		// a error occurs, and some heading comes as
		// # 1. Test
		// ### 1.0.1. Test
		// that it will not fail
		for i := level - 1; i > 0; i-- {
			if levels[i] == -1 {
				levels[i] += 1
				continue
			}
			break
		}

		// pass the current header we have to InsertHeader.
		// pass the levels as well, but only the ones needed,
		// so if it contains [1,2,3] and we are at a level 1 header (0)
		// then only pass [1]
		h = InsertHeader(h, levels[:level+1], title, ID)

		return blackfriday.GoToNext
	})

	return h.Headings, nil
}

func InsertHeader(h Heading, levels []int, title, id string) Heading {
	// if we still need to nest inside our struct,
	// then do this
	if len(levels) > 0 {
		// if we need to create and headers, then create
		// all the required ones
		for i := len(h.Headings); i <= levels[0]; i++ {
			h.Headings = append(h.Headings, Heading{})
		}

		// update the header at the required level
		h.Headings[levels[0]] = InsertHeader(h.Headings[levels[0]], levels[1:], title, id)

		// return the new header, recurse all the way up
		return h
	}

	// we are at our final destination, update header
	h.ID = id
	h.Title = title

	return h
}
