package site

import (
	"bytes"
	"fmt"
	"io"
	"log"

	blackfriday "gopkg.in/russross/blackfriday.v2"
)

type diagram int

const (
	DiagramNone diagram = iota
	DiagramPlantUML
	DiagramAsymptote
)

type customRenderer struct {
	htmlRenderer *blackfriday.HTMLRenderer
	p            *Page
}

func newCustomRenderer(page *Page) *customRenderer {
	return &customRenderer{
		p: page,
		htmlRenderer: blackfriday.NewHTMLRenderer(blackfriday.HTMLRendererParameters{
			Flags: blackfriday.CommonHTMLFlags & blackfriday.SmartypantsFractions,
		}),
	}
}

func (c *customRenderer) RenderNode(w io.Writer, node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
	if node.Type == blackfriday.CodeBlock && (bytes.Equal(node.CodeBlockData.Info, []byte("plantuml")) || bytes.Equal(node.CodeBlockData.Info, []byte("asy"))) {
		blockInfo := string(node.CodeBlockData.Info)
		var dType diagram
		switch blockInfo {
		case "plantuml":
			dType = DiagramPlantUML
		case "asy":
			dType = DiagramAsymptote
		default:
			dType = DiagramNone
		}

		if dType > DiagramNone {
			diaName := "diagrams/" + generateSha1(node.Literal) + ".svg"
			fileName := "diagrams/" + generateSha1(node.Literal)

			if exists, err := fileExists(c.p.s.Output + diaName); err != nil || !exists {
				var diaBytes []byte
				var err error
				if dType == DiagramPlantUML {
					diaBytes, err = makePlantuml(node.Literal)
					fileName += ".plantuml"
				} else if dType == DiagramAsymptote {
					diaBytes, err = makeAsy(node.Literal)
					fileName += ".asy"
				} else {
					log.Fatal("There is no gerenation for this diagcam")
				}

				if err != nil {
					fmt.Println("Failed to make diagram for:", string(node.Literal))
					return blackfriday.GoToNext
				}

				c.p.ByteAssets[diaName] = diaBytes
				c.p.ByteAssets[fileName] = node.Literal
			}

			pNode := blackfriday.NewNode(blackfriday.Image)
			pNode.LinkData.Destination = []byte(c.p.s.Prefix + "/" + diaName)

			// insert our nodes, open and close
			c.htmlRenderer.RenderNode(w, pNode, true)
			return c.htmlRenderer.RenderNode(w, pNode, false)
		}
	}

	return c.htmlRenderer.RenderNode(w, node, entering)
}

func (c *customRenderer) RenderHeader(w io.Writer, ast *blackfriday.Node) {
	c.htmlRenderer.RenderHeader(w, ast)
}

func (c *customRenderer) RenderFooter(w io.Writer, ast *blackfriday.Node) {
	c.htmlRenderer.RenderFooter(w, ast)
}
