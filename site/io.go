package site

import (
	"errors"
	"io"
	"os"
	"strings"
)

func fileExists(filename string) (bool, error) {
	f, err := os.Open(filename)
	if err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}
		return false, err
	}
	defer f.Close()

	return true, nil
}

func ensureParentFolder(filename string) error {
	if index := strings.LastIndex(filename, "/"); index != -1 {
		return os.MkdirAll(filename[:index], os.ModePerm)
	}

	return nil
}

func copyFolder(src, dst string) error {
	// open source
	osrc, err := os.Open(src)
	if err != nil {
		return err
	}
	defer osrc.Close()

	// ensure it is a directory
	lsrc, _ := osrc.Stat()
	if !lsrc.IsDir() {
		return errors.New("Not a directory")
	}

	// make directory in dst
	if err := os.MkdirAll(dst, lsrc.Mode()); err != nil {
		return err
	}

	// loop entries in folder
	entries, err := osrc.Readdir(-1)
	if err != nil {
		return err
	}

	for _, entry := range entries {
		cursrc := src + "/" + entry.Name()
		curdst := dst + "/" + entry.Name()

		if entry.IsDir() {
			if err := copyFolder(cursrc, curdst); err != nil {
				return err
			}
			continue
		}

		if err := copyFile(cursrc, curdst); err != nil {
			return err
		}
	}

	return nil
}

func copyFile(src, dst string) error {
	// read file
	ifi, err := os.Open(src)
	if err != nil {
		return err
	}
	defer ifi.Close()

	var bs []byte
	buffer := make([]byte, 4096)
	for {
		n, err := ifi.Read(buffer)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		bs = append(bs, buffer[:n]...)
	}

	// get inputfile stats
	istat, _ := ifi.Stat()

	// ensure parent directory exists
	if err := ensureParentFolder(dst); err != nil {
		return err
	}

	// write file
	ofi, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer ofi.Close()

	// set correct permissions
	if err := ofi.Chmod(istat.Mode()); err != nil {
		return err
	}

	if _, err := ofi.Write(bs); err != nil {
		return err
	}
	return nil
}
