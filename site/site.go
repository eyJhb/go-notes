package site

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"os"
	"strings"
)

type Site struct {
	Name string `yaml:"name"`

	Prefix   string `yaml:"prefix"` // e.g. if site is prefixed with `notes/index.html`
	Input    string `yaml:"input"`
	Output   string `yaml:"output"`
	Assets   string `yaml:"assets"`
	Template string `yaml:"template"`

	IndexPage Page   `yaml:"index_page"`
	Pages     []Page `yaml:"pages"`

	templates map[string]*template.Template
}

func (s *Site) readyTemplates() error {
	// ensure not loading multiple times
	if len(s.templates) == 0 {
		s.templates = readyTemplate(s.Template)
	}

	return nil
}

func (s *Site) Generate() error {
	// ready our templates
	if err := s.readyTemplates(); err != nil {
		return err
	}

	// generate index page, if we have any
	if s.IndexPage.Path != "" {
		if err := s.walkPages(s.IndexPage); err != nil {
			return err
		}
	}

	// walkPages (generates html + copies assets)
	for _, page := range s.Pages {
		if err := s.walkPages(page); err != nil {
			return err
		}
	}

	// copy site assets to output folder
	if s.Assets != "" {
		if err := copyFolder(s.Assets, s.Output); err != nil {
			return err
		}
	}

	return nil
}

func (s *Site) walkPages(p Page) error {
	// set the site for the page
	p.s = s

	// get html
	pHtml, err := p.HTML(s.Prefix)
	if err != nil {
		return err
	}

	// get headings (current page)
	pHeadings, err := p.Headings()
	if err != nil {
		return err
	}

	// place into struct for template
	pageData := struct {
		Title       string
		Prefix      string
		RootPages   []Page
		CurrentPage Page
		Headings    []Heading
		Content     template.HTML
	}{
		Title:       s.Name,
		Prefix:      s.Prefix,
		RootPages:   s.Pages,
		CurrentPage: p,
		Headings:    pHeadings,
		Content:     template.HTML(pHtml),
	}

	// execute template
	buf := bytes.Buffer{}
	if err := s.templates["base"].ExecuteTemplate(&buf, "base", pageData); err != nil {
		return err
	}

	// ensure dst path exists
	if err := ensureParentFolder(s.Output + "/" + p.Path); err != nil {
		return err
	}

	// save into dst folder
	ofi, err := os.Create(s.Output + "/" + strings.Replace(p.Path, ".md", ".html", 1))
	if err != nil {
		return err
	}
	defer ofi.Close()

	if _, err := ofi.Write(buf.Bytes()); err != nil {
		return err
	}

	// copy assets
	for _, asset := range p.Assets {
		// stat asset src
		assetStat, err := os.Stat(s.Input + "/" + asset)
		if err != nil {
			return err
		}

		if assetStat.IsDir() {
			if err := copyFolder(s.Input+"/"+asset, s.Output+"/"+asset); err != nil {
				return err
			}
		} else {
			if err := copyFile(s.Input+"/"+asset, s.Output+"/"+asset); err != nil {
				return err
			}
		}
	}

	// copy byte assets
	for name, data := range p.ByteAssets {
		name = s.Output + name

		if err := ensureParentFolder(name); err != nil {
			return err
		}

		if err := ioutil.WriteFile(name, data, 0644); err != nil {
			return err
		}
	}

	// execute all subtemplates
	for _, page := range p.Pages {
		if err := s.walkPages(page); err != nil {
			return err
		}
	}

	return nil
}
