package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"gitlab.com/eyJhb/go-notes/site"
	yaml "gopkg.in/yaml.v2"
)

const (
	CONF_ENV_PREFIX = "GO_NOTES_"
)

func main() {
	// get file
	if len(os.Args) < 2 {
		log.Fatal("Call with argument of configuration file")
	}

	confFile := os.Args[1]

	fmt.Printf("Loading config file: %s\n", confFile)

	// read file
	confBytes, err := ioutil.ReadFile(confFile)
	if err != nil {
		log.Fatal(err)
	}

	var s site.Site
	if err := yaml.Unmarshal(confBytes, &s); err != nil {
		log.Fatal(err)
	}

	s.Name = getEnvConf("NAME", s.Name)
	s.Prefix = getEnvConf("PREFIX", s.Prefix)
	s.Input = getEnvConf("INPUT", s.Input)
	s.Output = getEnvConf("OUTPUT", s.Output)
	s.Assets = getEnvConf("ASSETS", s.Assets)
	s.Template = getEnvConf("TEMPLATE", s.Template)

	// lets append the location of the
	// config file to the input/output dirs, so
	// that the files are relative to the config file
	if index := strings.LastIndex(confFile, "/"); index != -1 {
		s.Input = confFile[:index] + "/" + s.Input
		s.Output = confFile[:index] + "/" + s.Output
		s.Template = confFile[:index] + "/" + s.Template
		s.Assets = confFile[:index] + "/" + s.Assets
	}

	if err := s.Generate(); err != nil {
		log.Fatal(err)
	}
}

func getEnvConf(key, defaultValue string) string {
	if val := os.Getenv(CONF_ENV_PREFIX + key); val != "" {
		return val
	}
	return defaultValue
}
